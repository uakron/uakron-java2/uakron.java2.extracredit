import java.util.Scanner;

public class MultithreadingMath
{
	private static int partialSum = 0, factorial = 1, sum, difference, product;
	private static double quotient;
	
	public static void main(String[] args) throws InterruptedException
	{
		Scanner input = new Scanner(System.in);
		
		System.out.printf("Enter first number: ");
		int num1 = input.nextInt();
		
		System.out.printf("Enter second number: ");
		int num2 = input.nextInt();
		
		input.close();
		
		Thread thread1 = performCalculation(num1, Calculation.PartialSum);
		Thread thread2 = performCalculation(num2, Calculation.Factorial);
		
		thread1.start();
		thread2.start();
		
		thread1.join();
		thread2.join();
		
		Thread[] threads = 
		{
			getValue(Calculation.Addition),
			getValue(Calculation.Subtraction),
			getValue(Calculation.Multiplication),
			getValue(Calculation.Division)
		};
		
		for (Thread thread: threads) {
			thread.start();
		}
		
		for (Thread thread: threads) {
			thread.join();
		}
		
		System.out.println("Sum from 1 to first number provided: " + partialSum);
		System.out.println("Multiplication from 1 to second number provided: " + factorial);
		System.out.println("Partial sum + factorial: " + sum);
		System.out.println("Partial sum - factorial: " + difference);
		System.out.println("Partial sum * factorial: " + product);
		System.out.println("Partial sum / factorial: " + quotient);
	}

	private static Thread performCalculation(int num1, Calculation type)
	{
		return new Thread(new Runnable()
		{
		    private int value;
		    
		    { this.value = num1; }
		    
			public void run()
			{
				switch (type)
				{
					case PartialSum:
						for (int i = 1; i <= value; i++) { partialSum += i; };
						break;
					case Factorial:
						for (int i = 1; i <= value; i++) { factorial *= i; };
						break;
					default:
						break;
				}
			}
		});
	}
	
	private static Thread getValue(Calculation type) {
		return new Thread(new Runnable()
		{
			public void run()
			{
				switch (type)
				{
					case Addition:
						sum = partialSum + factorial;
						break;
					case Subtraction:
						difference = partialSum - factorial;
						break;
					case Multiplication:
						product = partialSum * factorial;
						break;
					case Division:
						quotient = (double) partialSum / factorial;
						break;
					default:
						break;
				}
			}
		});
	}
}

enum Calculation { PartialSum, Factorial, Addition, Subtraction, Multiplication, Division }